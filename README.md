# Setting up Kafka

Before we dive into creating a Kafka topic, we need to have a Kafka cluster running. We can achieve that using Docker and the Confluent Platform Docker images.

## Pre requirement 

### Install Docker and Docker Compose

To get started, you need to install Docker and Docker Compose on your machine. You can download and install Docker from the [official Docker website](https://www.docker.com/), and Docker Compose comes with Docker Desktop for Mac and Windows. On Linux, you can install Docker Compose using your distribution's package manager.


### Start the Confluent Platform

In order to start the environment use this docker-compose file and execute 

``` bash
docker-compose -f kafka.yaml up -d
```
This command starts a single-node Kafka cluster with one broker, one ZooKeeper instance, and the Confluent Control Center. It may take a few moments for the services to start up.

to stop the cluster user : 

``` bash
docker-compose -f kafka.yaml down -v
```
## Environment presentation 

This is a docker-compose file that describes a Kafka cluster using the Confluent Platform images. The cluster consists of several services, including:
- Zookeeper: This is a component of the Kafka cluster that provides distributed coordination between brokers.
- Broker: This is the Kafka broker service that handles the ingestion and processing of data.
- Schema Registry: This is the service that manages schemas for Kafka topics.
- Connect: This is the Kafka Connect service, which enables the integration of Kafka with external systems.
- Base: This is a base image for Kafka Connectors that provides common libraries and dependencies.
- Control Center: This is a web-based UI for managing and monitoring Kafka clusters.

Each service is defined with its own configuration, including environment variables and ports.

The Kafka broker is configured to use Zookeeper for coordination and to advertise its listeners on port 2181.

The Schema Registry is configured to use the Kafka broker for storage and to listen on port 29092.

The Connect service is configured to use the Kafka broker for coordination and to advertise its listeners on port 8083.

The Control Center service is configured to use the Kafka broker and Schema Registry, and to listen on port 9021.

To have an overview you can check the [Control center](http://localhost:9021)


The base container contains Kafka Cli binaries used to interact with the cluster. To use them use the following command line : 
``` bash
docker exec -it base /bin/sh
``` 
or use
``` bash
docker exec base CMD
``` 

## Mono partition 


1. Start by listing broker topic list : 

``` bash
docker exec base kafka-topics --bootstrap-server broker:29092 --list
```
2. Creating a Kafka topic
``` bash
docker exec base kafka-topics --bootstrap-server broker:29092 --create --partitions 1 --replication-factor 1 --topic test
```
This command creates a topic named `test` with one partition (`--partitions 1`) and one replica (`--replication-factor 1`).

3. Producing data to a Kafka topic

Now that we have a topic, let's produce some data to it. We'll use the `kafka-console-producer` tool to do this.

``` bash
docker exec -it base kafka-console-producer --bootstrap-server broker:29092 --topic test
```

This command starts a producer that publishes data to the `my-topic` topic. We're using the `--broker-list` option to specify the broker that we want to produce to.

Once the producer is running, you can start typing messages. Each message should be entered on a new line, and you can stop producing messages by pressing Ctrl+C.

4. Consuming data from a Kafka topic

Finally, let's consume the data that we just produced. We'll use the `kafka-console-consumer` tool to do this.

``` bash
docker exec base kafka-console-consumer --topic test --from-beginning  --bootstrap-server broker:29092
```
By default, a console consumer start at the end of the log : `--from-beginning` force it to start at the beginning... and is equivalent to `--offset latest`

### Consumer groups and offsets 

Check consumers page in [Control center](http://localhost:9021)

You can see a consumer group with and id like `console-consumer-xxxxx`

exec the same command (console consumer), and Check consumers page in [Control center](http://localhost:9021)

1. Name consumer group 
``` bash
kafka-console-consumer --bootstrap-server broker:29092 --from-beginning --topic test --group patate
```
The consumer read all messages from the begining

Stop it and restart :
``` bash
kafka-console-consumer --bootstrap-server broker:29092 --from-beginning --topic test --group patate
```
As a consumer group with this name already exist the consumer ignore the `--from-beginning` and restart from the last known position (offset) 

2. Start a new CLI
Produce new messages
=> The consumer continue reading
Kill the consumer

3. Produce new messages 
=> in CC => See lag

4. Start consumer 
=> Resume message consumption , Lag 0

## Multiple partition 

1. From within the terminal window create a Topic manually with Kafka’s command-line tool, specifying
that it should have two partitions :
``` bash
kafka-topics --bootstrap-server broker:29092 --create --topic two-p-topic --partitions 2 --replication-factor 1
```
2. You can use the kafka-topics tool to describe the details of the topic (1):
``` bash
kafka-topics --bootstrap-server broker:29092 --describe --topic two-p-topic
```
Which will result in this output:
```
Topic: two-p-topic PartitionCount: 2 ReplicationFactor: 1 Configs:
Topic: two-p-topic Partition: 0 Leader: 1 Replicas: 1 Isr: 1
Topic: two-p-topic Partition: 1 Leader: 1 Replicas: 1 Isr: 1
```
3. Use the command-line Producer to write several lines of data to the Topic
``` bash
for i in `seq 1 100`; do echo "$i:I am payload number $i"; done > numlist 
kafka-console-producer --broker-list broker:29092 --topic two-p-topic –producer-property linger.ms=100 batch.size=50 --property parse.key=true --property key.separator=: < numlist
```
4. Use the command-line Consumer to read the Topic
``` bash
kafka-console-consumer --bootstrap-server broker:9092 --from-beginning --topic two-p-topic
```
Press CTRL-c to exit the consumer

5. Note the order of the numbers. Rerun the Producer command in step 3, then rerun the Consumer
   command in step 4 and see if you observe any difference in the output.
1. What do you think is happening as the data is being written?
1. Try creating a new Topic with three Partitions and running the same steps again.

Sizing [Partions sizing](https://eventsizer.io/partitions)

[Partition consumption explain](https://softwaremill.com/kafka-visualisation/)


## Kafka Connect


### Install connectors

In order to install connectors go to [Confluent Hub](https://www.confluent.io/hub/) and download [JDBC Source and Sink conenctor](https://www.confluent.io/hub/confluentinc/kafka-connect-jdbc) in data/connect-jars
[JDBC Source Connector Configuration ](https://docs.confluent.io/kafka-connectors/jdbc/current/source-connector/source_config_options.html)

Available plugin can be list using the REST API : http://localhost:8083/connector-plugins/

IF nothing is installed, something went wrong, check *connect* container logs. If the problem is link to enterprise proxy use the offline install

#### Install while offline (using a ZIP file)

If you are offline, you can install a connector by downloading the ZIP file from Confluent Hub. To install the connector, run the following command:
```bash
confluent-hub install <component>-<version>.zip
```
For example :


Inside the *connect* container run something like that to download connectors jar :

```bash
curl https://d1i4a15mxbxib1.cloudfront.net/api/plugins/confluentinc/kafka-connect-jdbc/versions/10.7.1/confluentinc-kafka-connect-jdbc-10.7.1.zip --output confluentinc-kafka-connect-jdbc-10.7.1.zip
```
```bash
curl https://d1i4a15mxbxib1.cloudfront.net/api/plugins/mongodb/kafka-connect-mongodb/versions/1.10.0/mongodb-kafka-connect-mongodb-1.10.0.zip --output mongodb-kafka-connect-mongodb-1.10.0.zip  
```

```bash
confluent-hub install confluentinc-kafka-connect-jdbc-10.7.1.zip
```

If you want to use the Confluent Hub installer to d/l component, but make them available
when running this offline, spin up the stack once and then run :
```bash
 docker cp connect:/usr/share/confluent-hub-components ./data/connect-jars
```

#### Restart the connector container

After you have installed the connector, you must restart the connector container. If you do not restart the connector container, the installed connector tile will not be displayed on the Connect page in Confluent Control Center.


### Start a postgreSQL data base: 
1. create PG Database, (Adminer)[http://localhost:8080] can be used to monitor DB
Un-comment the postgres block in docker compose.yaml 

* User : *postgres*
* PWD : *secret*
* DB : *postgres*
```bash 
docker exec -it  pg-postgres psql -U postgres
```
2. Create a new table
```sql
create table public.operators(id integer not null constraint id primary key, name varchar not null,description varchar default '42'::character varying not null,legacy boolean default true not null,created_on timestamp default CURRENT_TIMESTAMP not null,updated_on timestamp default CURRENT_TIMESTAMP not null);

alter table public.operators owner to postgres;
```
3. Run the following statements to insert the operators in the corresponding table:
```sql
   insert into operators(id, name) values(6, 'Oy Pohjolan Liikenne Ab');
   insert into operators(id, name) values(12, 'Helsingin Bussiliikenne Oy');
   insert into operators(id, name) values(17, 'Tammelundin Liikenne Oy');
   insert into operators(id, name) values(18, 'Pohjolan Kaupunkiliikenne Oy');
   insert into operators(id, name) values(19, 'Etelä-Suomen Linjaliikenne Oy');
   insert into operators(id, name) values(20, 'Bus Travel Åbergin Linja Oy');
   insert into operators(id, name) values(21, 'Bus Travel Oy Reissu Ruoti');
   insert into operators(id, name) values(22, 'Nobina Finland Oy');
   insert into operators(id, name) values(36, 'Nurmijärven Linja Oy');
   insert into operators(id, name) values(40, 'HKL-Raitioliikenne');
   insert into operators(id, name) values(45, 'Transdev Vantaa Oy');
   insert into operators(id, name) values(47, 'Taksikuljetus Oy');
   insert into operators(id, name) values(51, 'Korsisaari Oy');
   insert into operators(id, name) values(54, 'V-S Bussipalvelut Oy');
   insert into operators(id, name) values(55, 'Transdev Helsinki Oy');
   insert into operators(id, name) values(58, 'Koillisen Liikennepalvelut Oy');
   insert into operators(id, name) values(59, 'Tilausliikenne Nikkanen Oy');
   insert into operators(id, name) values(90, 'VR Oy');
```
4. Double-check the records are in the table:
```sql
select * from operators;
```

Exit psql and the container postgres by pressing CTRL-d.


6. Deploy a connector
```bash 
curl -s -X POST -H "Content-Type: application/json" --data @data/operator-connector.json http://localhost:8083/connectors
```
7. Use postman to check the connector

[Kafka Connect REST API](https://docs.confluent.io/platform/current/connect/references/restapi.html) or use Control center.
http://localhost:8083/connectors/OperatorsConnectorSource/status

8. Check the topic
```bash 
kafka-avro-console-consumer --bootstrap-server broker:9092 --property schema.registry.url=http://schema-registry:8081 --topic pg-operators --from-beginning
```

10. JDBC modes

Have a look at [JDBC modes](https://docs.confluent.io/kafka-connectors/jdbc/current/source-connector/source_config_options.html#mode)
Change it accordly to detect modification on existing rows

### Add MongoDB data base:
Un-comment the MongoDB block in docker compose.yaml 

1. Create the sink connector ;-)


# Cleanup
docker image prune
docker volume prune